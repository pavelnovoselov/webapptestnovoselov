﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PuppeteerSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebAppConverter.Models;

namespace WebAppConverter.Controllers
{
    public class HomeController : Controller
    {
        private readonly IWebHostEnvironment _appEnvironment;
        public HomeController(IWebHostEnvironment appEnvironment)
        {
            _appEnvironment = appEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Сonversion(List<IFormFile> uploadedFile)
        {
            if (uploadedFile != null)
            {
                string filePath = Path.Combine(_appEnvironment.ContentRootPath, uploadedFile[0].FileName);

                SaveFile(uploadedFile[0], filePath);                

                await Converting(filePath);

                string file_path = Path.Combine(_appEnvironment.ContentRootPath, "output.pdf");
                string file_type = "application/pdf";
                string file_name = "output.pdf";
                return PhysicalFile(file_path, file_type, file_name);
            }

            return Ok();
        }

        private void SaveFile(IFormFile file, string filePath)
        {            
            using var strem = System.IO.File.Create(filePath);
            file.CopyTo(strem);

        }

        private async Task Converting(string filePath)
        {
            var browserFetcher = new BrowserFetcher();

            await browserFetcher.DownloadAsync();
            await using var browser = await Puppeteer.LaunchAsync(new LaunchOptions { Headless = true });

            await using var page = await browser.NewPageAsync();

            await page.GoToAsync(filePath);

            await page.PdfAsync("output.pdf", new PdfOptions { PrintBackground = true });
        }
    }
}
